
module.exports.Vars = require('./lib/VariableStore');
module.exports.Variable = require('./lib/Variable');

module.exports.layout = {
  FilteredBooleanQuery: require('./layout/FilteredBooleanQuery'),
  DisMaxBooleanQuery: require('./layout/DisMaxBooleanQuery'),
};

module.exports.view = {
  focus: require('./view/focus'),
  popularity: require('./view/popularity'),
  population: require('./view/population'),
  localregions: require('./view/localregions'),
  ngrams: require('./view/ngrams'),
  ngrams_multi: require('./view/ngrams_multi'),
  phrase: require('./view/phrase'),
  phrase_multi: require('./view/phrase_multi'),
  doc_type: require( './view/doc_type' ),
  address: require('./view/address'),
  admin: require('./view/admin'),
  admin_multi_match: require('./view/admin_multi_match'),
  multi_match: require('./view/multi_match'),
  boundary_circle: require('./view/boundary_circle'),
  boundary_rect: require('./view/boundary_rect'),
  boundary_country: require('./view/boundary_country'),
  sort_distance: require('./view/sort_distance'),
  sort_numeric_script: require('./view/sort_numeric_script'),
  sources: require('./view/sources')
};

module.exports.defaults = require('./defaults');
