// An object of options to indicate where to post to
var post_options = {
    host: 'loclhost',
    port: '9200',
    path: '/pelias/_search',
    method: 'POST'
    // headers: {
    //     'Content-Type': 'application/x-www-form-urlencoded',
    //     'Content-Length': Buffer.byteLength(post_data)
    // }
};

// Set up the request
var post_req = http.request( post_options, function( res ) {
    res.setEncoding( 'utf8' );
    res.on('data', function (chunk) {
        console.log('Response: ' + chunk);
    });
});