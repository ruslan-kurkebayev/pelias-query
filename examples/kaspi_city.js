
var query = require('../index'),
    vs = new query.Vars( query.defaults ),
    q = new query.layout.FilteredBooleanQuery();

// 'абая алматы' -> пр абая алматы
// 'абая иргели' -> ул абая иргели
vs.var( 'input:name', 'абая алматы' );
// vs.set({ 'focus:point:lat': 1, 'focus:point:lon': 2 });
// vs.set({ 'input:housenumber': '101' });
// vs.set({ 'boundary:circle:lat': 1, 'boundary:circle:lon': 2 });

vs.set({
  'boundary:top_left:lat': 43.403222,
  'boundary:top_left:lon': 76.741525,
  'boundary:bottom_right:lat': 43.038854,
  'boundary:bottom_right:lon': 77.169359,
});

// vs.set({ 'boundary:country': 'USA' });
// vs.set({ 'input:country': 'Казахстан' });
// vs.set({ 'input:city': 'Иргели' });

// main mathes
q.score( query.view.ngrams_multi( 'names' ), 'must' );
q.score( query.view.phrase_multi( 'names' ), 'must' );

// scoring boost
// q.score( query.view.focus );

// address components
q.score( query.view.address('housenumber') )
 .score( query.view.address('street') )
 .score( query.view.address('postcode') );

// document type scoring
q.score( query.view.doc_type( 'region' ) )
	.score( query.view.doc_type( 'way' ) )
	.score( query.view.doc_type( 'city' ) )
	.score( query.view.doc_type( 'county' ) )
	.score( query.view.doc_type( 'town' ) )
	.score( query.view.doc_type( 'neighbourhood' ) )
	.score( query.view.doc_type( 'village' ) )
	.score( query.view.doc_type( 'venue' ) )
	.score( query.view.doc_type( 'address' ) );

// admin components
q.score( query.view.phrase_multi( 'admin' ) );

// non-scoring hard filters
// q.filter( query.view.boundary_circle )
q.filter( query.view.boundary_rect );

// sorting 'tie-breakers'
q.sort( query.view.sort_distance );

var rendered = q.render( vs );
console.log( JSON.stringify( rendered, null, 2 ) );