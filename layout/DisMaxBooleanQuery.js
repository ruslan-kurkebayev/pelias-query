/*
  DIS_MAX query renderer. Probably should not be used with 'should' scores only.
  Helpful when there are many 'must' scores and we should match at least one of them.
*/
function Layout(){
  this._score_must = [];
  this._score_should = [];
  this._filter = [];
  this._sort = [];
}

Layout.prototype.score = function( view, operator ){
  if ( operator === 'must' ){
    this._score_must.push( view );
  } else {
    this._score_should.push( view );
  }

  return this;
};

Layout.prototype.filter = function( view ){
  this._filter.push( view );

  return this;
};

Layout.prototype.sort = function( view ){
  this._sort.push( view );

  return this;
};

/*
Get array of 'should' scores
*/
Layout.prototype.get_should = function( vs ){
  var shouldScores = [];
  this._score_should.forEach( function( view ){
    var rendered = view( vs );
    if( rendered ){
      shouldScores.push( rendered );
    }
  });

  return shouldScores;
};

/*
  Add 'should' scores into each 'bool' query with 'must' scores
*/
Layout.prototype.render_should = function( vs, q ){
  var shouldScores = this.get_should( vs );
  if ( shouldScores.length > 0 ){
    q.query.dis_max.queries.forEach( function ( boolQuery ){
      boolQuery.bool[ 'should' ] = shouldScores;
    } );
  }

  return q;
};

/*
Get array of 'should' scores
*/
Layout.prototype.get_filter = function( vs ){
  var filterScores = [];
  this._filter.forEach( function( view ){
    var rendered = view( vs );
    if( rendered ){
      filterScores.push( rendered );
    }
  });

  return filterScores;
};

/*
  Add 'filter' scores into each 'bool' query with 'must' scores
*/
Layout.prototype.render_filter = function( vs, q ){
  var filterScores = this.get_filter( vs );
  if( filterScores.length > 0 ){
    q.query.dis_max.queries.forEach( function ( boolQuery ){
      boolQuery.bool.filter = filterScores;
    } );
  }

  return q;
};

Layout.prototype.abc = function( arg ){
  console.log( arg );
};

Layout.prototype.render_must_not = function( thisObj, vs, view ){
  var mustNots = [];
  thisObj._score_must.forEach( function( viewOther ){
    if ( viewOther !== view ){
      var renderedOther = viewOther( vs );
      if ( renderedOther ){
        mustNots.push( renderedOther );
      }
    }
  });

  return mustNots;
};

/*
Put each 'must' score into it's own 'bool' query
*/
Layout.prototype.render_must = function( thisObj, vs, q ){
  if( thisObj._score_must.length > 0 ){
    // For each 'must' score create a query
    thisObj._score_must.forEach( function( view ){
      var query = { bool: {} };
      var rendered = view( vs );
      if( rendered ){
        query.bool.must = [];
        query.bool.must.push( rendered );
        // console.log( this.toString() );
        // console.log( this instanceof Array  );
        // console.log( this.prototype  );
        var mustNots = thisObj.render_must_not( thisObj, vs, view );
        if ( mustNots.length > 0 ){
          query.bool.must_not = mustNots;
        }
        q.query.dis_max.queries.push( query );
      }
    });
  }

  return q;
};

Layout.prototype.render = function( vs ){
  var q = Layout.base( vs );
  if ( this._score_must.length > 0 ){
    // for each 'must' score create a 'bool' query
    q = this.render_must( this, vs, q );

    // for each 'bool' query with 'must' add 'should' scores
    q = this.render_should( vs, q );

    // handle filter views under 'filter' section (only 'must' is allowed here)
    q = this.render_filter( vs, q );


    // // handle sorting views under 'sort'
    // if( this._sort.length ){
    //   this._sort.forEach( function( view ){
    //     if( !Array.isArray( q.sort ) ){
    //       q.sort = [];
    //     }
    //     var rendered = view( vs );
    //     if( rendered ){
    //       q.sort.push( rendered );
    //     }
    //   });
    // }
  }

  return q;
};

Layout.base = function( vs ){
  return {
    query: {
      dis_max: {
        queries: []
      }
    },
    size: vs.var('size'),
    track_scores: vs.var('track_scores'),
    sort: ['_score']
  };
};

module.exports = Layout;