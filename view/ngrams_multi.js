/**
  this view is wrapped in a function so it can be re-used
**/
module.exports = function( field_group ){
	return function( vs ){

	  // validate required params
	  if( !field_group ||
	  		!vs.isset('input:name') ||
	      !vs.isset('ngrams_multi:' + field_group + ':type') ||
	      !vs.isset('ngrams_multi:' + field_group + ':analyzer') ||
	      !vs.isset('ngrams_multi:' + field_group + ':fields') ||
	      !vs.isset('ngrams_multi:' + field_group + ':boost') ||
	      !vs.isset('ngrams_multi:' + field_group + ':fuzziness') ){
	    return null;
	  }

	  // base view
	  var view = { multi_match: {} };

	  // match query
	  view.multi_match = {
	  	type: vs.var( 'ngrams_multi:' + field_group + ':type' ),
	    query: vs.var( 'input:name' ),
	    fields: vs.var( 'ngrams_multi:' + field_group + ':fields' ),
	    analyzer: vs.var( 'ngrams_multi:' + field_group + ':analyzer' ),
	    boost: vs.var( 'ngrams_multi:' + field_group + ':boost' ),
	    fuzziness: vs.var( 'ngrams_multi:' + field_group + ':fuzziness' )
	  };

	  return view;
	};
};