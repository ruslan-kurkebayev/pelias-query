
module.exports = function( vs ){

  // validate required params
  if( !vs.isset('boundary:top_left:lat') ||
      !vs.isset('boundary:top_left:lon') ||
      !vs.isset('boundary:bottom_right:lat') ||
      !vs.isset('boundary:bottom_right:lon') ||
      !vs.isset('centroid:field') ){
    return null;
  }

  // base view
  var view = {
    geo_bounding_box: {}
  };

  // bbox
  view.geo_bounding_box[ vs.var('centroid:field') ] = {
    top_left: {
      lat:  vs.var('boundary:top_left:lat'),
      lon:  vs.var('boundary:top_left:lon')
    },
    bottom_right: {
      lat:  vs.var('boundary:bottom_right:lat'),
      lon:  vs.var('boundary:bottom_right:lon'),
    }
  };

  return view;
};