/**
  this view is wrapped in a function so it can be re-used
**/
module.exports = function( property ){
  return function( vs ){

    // validate required params
    if( !vs.isset( 'doc_type:' + property + ':boost' ) ){
      return null;
    }

    // base view
    var view = { match: { _type: { } } };

    // match query
    view.match._type.query = property;
    view.match._type.boost = vs.var('doc_type:' + property + ':boost');

    return view;
  };
};